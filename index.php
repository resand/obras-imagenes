<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8"/>
		<title>Obras y Acciones Propuestas</title>
		<meta name="viewport" content="width=320, initial-scale=1, maximum-scale=1, user-scalable=1"/>
		<meta http-equiv="X-UA-Compatible" content="IE=edge"/> 

		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
			<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
		<![endif]-->
		
		<script type="text/javascript">
		//<![CDATA[
		window.__CF=window.__CF||{};window.__CF.AJS={"ga_key":{"ua":"UA-714221-40","ga_bs":"2"}};
		//]]>
		</script>
		<script type="text/javascript">
		//<![CDATA[
		try{if (!window.CloudFlare) { var CloudFlare=[{verbose:0,p:0,byc:0,owlid:"cf",bag2:1,mirage2:0,oracle:0,paths:{cloudflare:"/cdn-cgi/nexp/abv=1309062649/"},atok:"b5f1cefee03f119b83750d6994ab207c",petok:"77096d1b5eb2836b6efb53ebd3a72a0f-1382461195-1800",zone:"mixitup.io",rocket:"0",apps:{"ga_key":{"ua":"UA-714221-40","ga_bs":"2"}}}];CloudFlare.push({"apps":{"ape":"f7ed20db0c1338e6702c9fd83ceda288"}});var a=document.createElement("script"),b=document.getElementsByTagName("script")[0];a.async=!0;a.src="//ajax.cloudflare.com/cdn-cgi/nexp/abv=3224043168/cloudflare.min.js";b.parentNode.insertBefore(a,b);}}catch(e){};
		//]]>
		</script>
		
		<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700|Open+Sans:300,600,700' rel='stylesheet' type='text/css'/>
		<link rel="shortcut icon" href="favicon.ico">
		<link rel="stylesheet" type="text/css" href="css/parks.css"/>
		
		<script src="js/jquery-1.9.1.min.js"></script>
		<script src="js/jquery-ui.sortable.min.js"></script>
		<script src="js/jquery.ui.touch-punch.min.js"></script>
		<script src="js/jquery.mixitup.min.js"></script>
		
		<script src="js/lightbox-2.6.min.js"></script>
		<script src="js/modernizr.custom.js"></script>
		<link href="css/lightbox.css" rel="stylesheet" />
		
		
		<script type="text/javascript">
			
			function imgLoaded(img){	
				$(img).parent().addClass('loaded');
			};

			// ON DOCUMENT READY:
			$(function(){
				
				// INSTANCIAMOS LA LIBRERIA MIXITUP
				
				$('#Obras').mixitup({
					layoutMode: 'list', // Iniciar en modo lista (display: block) por defecto
					listClass: 'list', // Clase Container para cuando está en modo de lista
					gridClass: 'grid', // Clase Container para cuando está en modo cuadricula
					effects: ['fade','blur'], // Lista de efectos
					listEffects: ['fade','rotateX'] // Lista de efectos sólo para el modo lista
				});
				
				// HANDLE LAYOUT CHANGES
				
				// Diseño & enlazar botones a los métodos toList y toGrid:

				
				$('#ToList').on('click',function(){
					$('.button').removeClass('active');
					$(this).addClass('active');
					$('#Obras').mixitup('toList');
				});

				$('#ToGrid').on('click',function(){
					$('.button').removeClass('active');
					$(this).addClass('active');
					$('#Obras').mixitup('toGrid');
				});
				
				// HANDLE MULTI-DIMENSIONAL CHECKBOX FILTERING
				
								
				var $filters = $('#Filters').find('li'),
					dimensions = {
						region: 'all', // Create string for first dimension
						localidad: 'all' // Create string for second dimension
					};
					
				// Bind checkbox click handlers:
				
				$filters.on('click',function(){
					var $t = $(this),
						dimension = $t.attr('data-dimension'),
						filter = $t.attr('data-filter'),
						filterString = dimensions[dimension];
						
					if(filter == 'all'){
						// If "all"
						if(!$t.hasClass('active')){
							// if unchecked, check "all" and uncheck all other active filters
							$t.addClass('active').siblings().removeClass('active');
							// Replace entire string with "all"
							filterString = 'all';	
						} else {
							// Uncheck
							$t.removeClass('active');
							// Emtpy string
							filterString = '';
						}
					} else {
						// Else, uncheck "all"
						$t.siblings('[data-filter="all"]').removeClass('active');
						// Remove "all" from string
						filterString = filterString.replace('all','');
						if(!$t.hasClass('active')){
							// Check checkbox
							$t.addClass('active');
							// Append filter to string
							filterString = filterString == '' ? filter : filterString+' '+filter;
						} else {
							// Uncheck
							$t.removeClass('active');
							// Remove filter and preceeding space from string with RegEx
							var re = new RegExp('(\\s|^)'+filter);
							filterString = filterString.replace(re,'');
						};
					};
					
					// Set demension with filterString
					dimensions[dimension] = filterString;
					
					// We now have two strings containing the filter arguments for each dimension:	
					console.info('dimension 1: '+dimensions.region);
					console.info('dimension 2: '+dimensions.localidad);
					
										
					$('#Obras').mixitup('filter',[dimensions.region, dimensions.localidad])			
				});

			});
		</script>
		
		
		<script type="text/javascript">
			/* <![CDATA[ */
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-714221-40']);
			_gaq.push(['_trackPageview']);

			(function() {
			var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
			ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
			var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();

			(function(b){(function(a){"__CF"in b&&"DJS"in b.__CF?b.__CF.DJS.push(a):"addEventListener"in b?b.addEventListener("load",a,!1):b.attachEvent("onload",a)})(function(){"FB"in b&&"Event"in FB&&"subscribe"in FB.Event&&(FB.Event.subscribe("edge.create",function(a){_gaq.push(["_trackSocial","facebook","like",a])}),FB.Event.subscribe("edge.remove",function(a){_gaq.push(["_trackSocial","facebook","unlike",a])}),FB.Event.subscribe("message.send",function(a){_gaq.push(["_trackSocial","facebook","send",a])}));"twttr"in b&&"events"in twttr&&"bind"in twttr.events&&twttr.events.bind("tweet",function(a){if(a){var b;if(a.target&&a.target.nodeName=="IFRAME")a:{if(a=a.target.src){a=a.split("#")[0].match(/[^?=&]+=([^&]*)?/g);b=0;for(var c;c=a[b];++b)if(c.indexOf("url")===0){b=unescape(c.split("=")[1]);break a}}b=void 0}_gaq.push(["_trackSocial","twitter","tweet",b])}})})})(window);
			/* ]]> */
		</script>
	</head>

	<body>
	<section>
		<!-- BEGIN HEADER -->
		<header class="ns">
			<div class="wrapper just wf">
				<a class="ib" href="#">
					<div id="Logo" class="ib">
						
					</div>
					<h1 class="ib"><strong>Obras</strong>/Gobierno del Estado de Campeche</h1>
				</a>
			</div>
		</header>
		
		<!-- END HEADER -->
		
		<!-- BEGIN DEMO WRAPPER -->
		
		<div class="wrapper wf">
			
			<!-- BEGIN CONTROLS -->
			
			<nav class="controls just">
				<div class="group" id="Sorts">
					<div class="button active" id="ToList"><i></i>Lista</div>
					<div class="button" id="ToGrid"><i></i>Cuadrícula</div>
				</div>
				<div class="group" id="Filters">
					<div class="drop_down wf">
						<span class="anim150">Municipio</span>
						<ul class="anim250">
							<li class="active" data-filter="all" data-dimension="region">Todos</li>
							<li data-filter="calakmul" data-dimension="region">Calakmul</li>
							<li data-filter="calkini" data-dimension="region">Calkiní</li>
							<li data-filter="campeche" data-dimension="region">Campeche</li>
							<li data-filter="candelaria" data-dimension="region">Candelaria</li>
							<li data-filter="carmen" data-dimension="region">Carmen</li>
							<li data-filter="champoton" data-dimension="region">Champotón</li>
							<li data-filter="escarcega" data-dimension="region">Escárcega</li>
							<li data-filter="hecelchakan" data-dimension="region">Hecelchakán</li>
							<li data-filter="hopelchen" data-dimension="region">Hopelchén</li>
							<li data-filter="palizada" data-dimension="region">Palizada</li>
							<li data-filter="tenabo" data-dimension="region">Tenabo</li>
						</ul>
					</div>
					<div class="drop_down wf">
						<span class="anim150">Localidad</span>
						<ul class="anim250">
							<li class="active" data-filter="all" data-dimension="localidad">Todos</li>
							<li data-filter="pablogarcia" data-dimension="localidad">Pablo García</li>
							<li data-filter="xbonil" data-dimension="localidad">Xbonil</li>
							<li data-filter="ninosheroes" data-dimension="localidad">Niños Héroes</li>
							<li data-filter="puebladorelia" data-dimension="localidad">Puebla de Morelia</li>
							<li data-filter="sanjose" data-dimension="localidad">San José (kilómetro 120)</li>
							<li data-filter="emilianozapata" data-dimension="localidad">Emiliano Zapata</li>
							<li data-filter="felipeangeles" data-dimension="localidad">Felipe Ángeles I</li>
							<li data-filter="echeverriacastellot" data-dimension="localidad">E. Echeverria Castellot I</li>
							<li data-filter="justosierra" data-dimension="localidad">Justo Sierra Mendez</li>
							<li data-filter="felipeangeles2" data-dimension="localidad">Felipe Ángeles II</li>
							<li data-filter="manuelcrescencio" data-dimension="localidad">Manuel Crescencio Rejon</li>
							<li data-filter="sanmiguel" data-dimension="localidad">San Miguel</li>
							<li data-filter="zohlaguna" data-dimension="localidad">Zoh-Laguna</li>
							<li data-filter="pakchen" data-dimension="localidad">Pakchen</li>
							<li data-filter="sancfcosuc-tuc" data-dimension="localidad">San Fco. Suc-Tuc</li>
							<li data-filter="crucerosanluis" data-dimension="localidad">Crucero San Luis</li>
							<li data-filter="xcupilcacab" data-dimension="localidad">Xcupil Cacab</li>
							<li data-filter="vicenteguerrero" data-dimension="localidad">Vicente Guerrero</li>
							<li data-filter="islasanisidro" data-dimension="localidad">Isla San Isidro</li>
							<li data-filter="rioblanco" data-dimension="localidad">Rio Blanco</li>
							<li data-filter="centaurodelnorte" data-dimension="localidad">Centauro del Norte</li>
							<li data-filter="lagunagrande" data-dimension="localidad">Laguna Grande</li>
							<li data-filter="miguelaleman" data-dimension="localidad">Miguel Alemán</li>
							<li data-filter="josemaroiapinosuarez" data-dimension="localidad">José María Pino Suarez</li>
							<li data-filter="nuevoparaiso" data-dimension="localidad">Nuevo Paraíso</li>
							<li data-filter="xmaben" data-dimension="localidad">X-maben</li>
							<li data-filter="mayatecun1" data-dimension="localidad">Mayatecun 1</li>
							<li data-filter="xbacab" data-dimension="localidad">Xbacab</li>
							<li data-filter="yohaltun" data-dimension="localidad">Yohaltun</li>
							<li data-filter="xpujil" data-dimension="localidad">Xpujil</li>
							<li data-filter="becan" data-dimension="localidad">Becan</li>
							<li data-filter="nuevocampeche" data-dimension="localidad">Nuevo Campeche</li>
							<li data-filter="pixoyal" data-dimension="localidad">Pixoyal</li>
							<li data-filter="miguelcolorado" data-dimension="localidad">Miguel Colorado</li>
							<li data-filter="migueldelamadrid" data-dimension="localidad">Lic Miguel de la Madrid</li>
							<li data-filter="guadalupevictoria" data-dimension="localidad">Guadalupe Victoria</li>
							<li data-filter="miguelhidalgoycostilla" data-dimension="localidad">Miguel Hidalgo y Costilla</li>
							<li data-filter="juandelacabadavera" data-dimension="localidad">Juan de la Cabada Vera</li>
							<li data-filter="pomuch" data-dimension="localidad">Pomuch</li>
							<li data-filter="sanmiguelallende" data-dimension="localidad">San Miguel Allende</li>
							<li data-filter="silvituc" data-dimension="localidad">Silvituc</li>
							

							
							

							

						</ul>
					</div>
				</div>
			</nav>
			
			<!-- END CONTROLS -->
			
			<!-- BEGIN OBRAS -->
			
			<ul id="Obras" class="just">
				
				<!-- "TABLE" CABECERA QUE CONTIENE BOTONES DE ORDENACIÓN (OCULTO EN EL MODO GRID)-->
				
				<!-- OBRAS EN CALAKMUL -->
				<div class="list_header">
					<div class="meta name active desc" id="SortByName">
						Obra &nbsp;
						<span class="sort anim150 asc active" data-sort="data-name" data-order="desc"></span>
						<span class="sort anim150 desc" data-sort="data-name" data-order="asc"></span>	
					</div>
					<div class="meta region">Municipio</div>
					<div class="meta rec">Localidad</div>
					<!--<div class="meta area" id="SortByArea">
						Area in Acres &nbsp;
						<span class="sort anim150 asc" data-sort="data-area" data-order="asc"></span>
						<span class="sort anim150 desc" data-sort="data-area" data-order="desc"></span>
					</div>-->
				</div>
				
				<!-- FAIL ELEMENT -->
				
				<div class="fail_element anim250">Lo sentimos &mdash; no hemos podido encontrar ninguna obra con estos criterios.</div>
				
				<!-- COMENZAR LISTA DE OBRAS (muchos de estos elementos sólo son visibles en modo de lista)-->
				
				<li class="mix calakmul pablogarcia" data-name="calakmul" data-area="47452.80">
					<div class="meta name">
						<a href="img/calakmul/pablo_garcia/1.jpg" data-lightbox="Obra1">
						<div class="img_wrapper">
							<img src="img/calakmul/pablo_garcia/1.jpg" height="453" width="607" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/pablo_garcia/2.jpg" data-lightbox="Obra1"></a>
						<a href="img/calakmul/pablo_garcia/3.jpg" data-lightbox="Obra1"></a>
						<div class="titles">
							<h2>REHABILITACIÓN Y PAVIMENTACIÓN DE <br> 1000 METROS LINEALES DE CALLES</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Pablo García</p>
					</div>
				</li>
				<li class="mix calakmul xbonil" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/xbonil/2.jpg" data-lightbox="Obra2">			
						<div class="img_wrapper">
							<img src="img/calakmul/xbonil/2.jpg" height="463" width="617" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/xbonil/1.jpg" data-lightbox="Obra2"></a>
						<a href="img/calakmul/xbonil/3.jpg" data-lightbox="Obra2"></a>
						<div class="titles">
							<h2>REHABILITACIÓN Y PAVIMENTACIÓN DE <br>1000 METROS LINEALES DE CALLES</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Xbonil</p>
					</div>
				</li>
				<li class="mix calakmul ninosheroes" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/ninios_heroes/2.jpg" data-lightbox="Obra3">	
						<div class="img_wrapper">
							<img src="img/calakmul/ninios_heroes/2.jpg" height="449" width="599" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/ninios_heroes/1.jpg" data-lightbox="Obra3"></a>
						<a href="img/calakmul/ninios_heroes/3.jpg" data-lightbox="Obra3"></a>
						<div class="titles">
							<h2>CONSTRUCCIÓN DE 700 ML DE CALLES A <br>NIVEL TERRACERÍA</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Niños Héroes</p>
					</div>
				</li>
				<li class="mix calakmul puebladorelia" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/puebla_morelia/1.jpg" data-lightbox="Obra4"></a>
						<div class="img_wrapper">
							<img src="img/calakmul/puebla_morelia/1.jpg" height="414" width="552" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/puebla_morelia/2.jpg" data-lightbox="Obra4"></a></a>
						<a href="img/calakmul/puebla_morelia/3.jpg" data-lightbox="Obra4"></a></a>
						<div class="titles">
							<h2>REHABILITACION Y PAVIMENTACION DE <br> 700 ML DE CALLES</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Puebla de Morelia</p>
					</div>
				</li>
				<li class="mix calakmul sanjose" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/san_jose/1.jpg" data-lightbox="Obra5">
						<div class="img_wrapper">
							<img src="img/calakmul/san_jose/1.jpg" height="460" width="614" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/san_jose/2.jpg" data-lightbox="Obra5"></a>
						<div class="titles">
							<h2>REHABILITACION Y PAVIMENTACION DE <br>1,000 ML DE CALLES</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>San José (kilómetro 120)</p>
					</div>
				</li>
				<li class="mix calakmul emilianozapata" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/emiliano_zapata/1.jpg" data-lightbox="Obra6">
						<div class="img_wrapper">
							<img src="img/calakmul/emiliano_zapata/1.jpg" height="452" width="602" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/emiliano_zapata/2.jpg" data-lightbox="Obra6"></a>
						<div class="titles">
							<h2>REHABILITACION Y PAVIMENTACION DE <br>1,000 ML DE CALLES</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Emiliano Zapata</p>
					</div>
				</li>
				<li class="mix calakmul felipeangeles" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/felipe_angeles/3.jpg" data-lightbox="Obra7">
						<div class="img_wrapper">
							<img src="img/calakmul/felipe_angeles/3.jpg" height="430" width="573" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/felipe_angeles/1.jpg" data-lightbox="Obra7"></a>
						<a href="img/calakmul/felipe_angeles/2.jpg" data-lightbox="Obra7"></a>
						<div class="titles">
							<h2>REHABILITACION Y PAVIMENTACION DE <br>1,000 ML DE CALLES</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Felipe Ángeles I</p>
					</div>
				</li>
				<li class="mix calakmul echeverriacastellot" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/echeverria_catellot/1.jpg" data-lightbox="Obra8">
						<div class="img_wrapper">
							<img src="img/calakmul/echeverria_catellot/1.jpg" height="438" width="584" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/echeverria_catellot/2.jpg" data-lightbox="Obra8"></a>
						<a href="img/calakmul/echeverria_catellot/3.jpg" data-lightbox="Obra8"></a>
						<div class="titles">
							<h2>REHABILITACION Y PAVIMENTACION DE <br>1,000 ML DE CALLES</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>E. Echeverria Castellot I</p>
					</div>
				</li>
				<li class="mix calakmul justosierra" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/justo_sierra/2.jpg" data-lightbox="Obra9">
						<div class="img_wrapper">
							<img src="img/calakmul/justo_sierra/2.jpg" height="483" width="644" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/justo_sierra/1.jpg" data-lightbox="Obra9"></a>
						<a href="img/calakmul/justo_sierra/3.jpg" data-lightbox="Obra9"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DE 1.75 KMS DE <br>CAMINO DE ACCESO A ZONAS DE <br> PRODUCCION A NIVEL TERRACERIA </h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Justo Sierra Mendez</p>
					</div>
				</li>
				<li class="mix calakmul felipeangeles2" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/felipe_angeles2/2.jpg" data-lightbox="Obra10">
						<div class="img_wrapper">
							<img src="img/calakmul/felipe_angeles2/2.jpg" height="405" width="540" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/felipe_angeles2/1.jpg" data-lightbox="Obra10"></a>
						<a href="img/calakmul/felipe_angeles2/3.jpg" data-lightbox="Obra10"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DE 2.45 KMS DE <br>CAMINO DE ACCESO A ZONAS DE <br> PRODUCCION A NIVEL TERRACERIA </h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Felipe Ángeles II</p>
					</div>
				</li>
				<li class="mix calakmul manuelcrescencio" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/manuel_crescencio/1.jpg" data-lightbox="Obra12">
						<div class="img_wrapper">
							<img src="img/calakmul/manuel_crescencio/1.jpg" height="403" width="538" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/manuel_crescencio/2.jpg" data-lightbox="Obra12"></a>
						<a href="img/calakmul/manuel_crescencio/3.jpg" data-lightbox="Obra12"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DE 1 KM DE <br>CAMINO DE ACCESO A ZONAS DE <br> PRODUCCION A NIVEL TERRACERIA </h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Manuel Crescencio Rejon</p>
					</div>
				</li>
				<li class="mix calakmul sanmiguel" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/san_miguel/2.jpg" data-lightbox="Obra13">
						<div class="img_wrapper">
							<img src="img/calakmul/san_miguel/2.jpg" height="398" width="531" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/san_miguel/1.jpg" data-lightbox="Obra13"></a>
						<a href="img/calakmul/san_miguel/3.jpg" data-lightbox="Obra13"></a>
						<div class="titles">
							<h2>CONSTRUCCIÓN DE 22 CAPTADORES <br>DE AGUA PLUVIAL CON CAPACIDAD<br> DE 12,000 LITROS</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>San Miguel</p>
					</div>
				</li>
				<li class="mix calakmul zohlaguna" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/zoh_laguna/puente/3.jpg" data-lightbox="Obra14">
						<div class="img_wrapper">
							<img src="img/calakmul/zoh_laguna/puente/3.jpg" height="433" width="578" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/zoh_laguna/puente/1.jpg" data-lightbox="Obra14"></a>
						<a href="img/calakmul/zoh_laguna/puente/2.jpg" data-lightbox="Obra14"></a>
						<div class="titles">
							<h2>CONSTRUCCION DE PUENTE VEHICULAR<br> UBICADO EN EL KM. 0+600 DEL CAMINO <br>ZOH-LAGUNA</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Zoh-Laguna</p>
					</div>
				</li>
				<li class="mix calakmul zohlaguna" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/zoh_laguna/camino/2.jpg" data-lightbox="Obra15">
						<div class="img_wrapper">
							<img src="img/calakmul/zoh_laguna/camino/2.jpg" height="441" width="588" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/zoh_laguna/camino/1.jpg" data-lightbox="Obra15"></a>
						<a href="img/calakmul/zoh_laguna/camino/3.jpg" data-lightbox="Obra15"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DE 1.8 KMS DE <br>CAMINO DE ACCESO A ZONAS DE <br>PRODUCCION A NIVEL TERRACERIA</h2>
							<p><em>H. Ayuntamiento de Calakmul</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Zoh-Laguna</p>
					</div>
				</li>
				
				<!-- OBRAS EN HOPELCHEN -->
				<li class="mix hopelchen pakchen" data-name="hopelchen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra16" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Construcción de Guarniciones y <br> Banquetas (1ª Etapa)</h2>
							<p><em>H. Ayuntamiento de Hopelchen, Dirección de Obras Públicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>Pakchen</p>
					</div>
				</li>
				<li class="mix hopelchen sancfcosuc-tuc" data-name="hopelchen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra17" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Construcción de Guarniciones y<br> Banquetas</h2>
							<p><em>H. Ayuntamiento de Hopelchen, Dirección de Obras Públicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>San Francisco Suc-Tuc</p>
					</div>
				</li>
				<li class="mix hopelchen crucerosanluis" data-name="hopelchen">
					<div class="meta name">
						<a href="img/hopelchen/crucero_san_luis/4.jpg" data-lightbox="Obra18">
						<div class="img_wrapper">
							<img src="img/hopelchen/crucero_san_luis/4.jpg" height="519" width="920" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/hopelchen/crucero_san_luis/1.jpg" data-lightbox="Obra18"></a>
						<a href="img/hopelchen/crucero_san_luis/2.jpg" data-lightbox="Obra18"></a>
						<a href="img/hopelchen/crucero_san_luis/3.jpg" data-lightbox="Obra18"></a>
						<div class="titles">
							<h2>Pavimentación de Calles</h2>
							<p><em>H. Ayuntamiento de Hopelchen, Dirección de Obras Públicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>Crucero San Luis</p>
					</div>
				</li>
				<li class="mix hopelchen sancfcosuc-tuc" data-name="hopelchen">
					<div class="meta name">
						<a href="img/hopelchen/san_fco_suc-tuc/guarniciones_banquetas/4.jpg" data-lightbox="Obra19">
						<div class="img_wrapper">
							<img src="img/hopelchen/san_fco_suc-tuc/guarniciones_banquetas/4.jpg" height="566" width="1004" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/hopelchen/san_fco_suc-tuc/guarniciones_banquetas/1.jpg" data-lightbox="Obra19"></a>
						<a href="img/hopelchen/san_fco_suc-tuc/guarniciones_banquetas/2.jpg" data-lightbox="Obra19"></a>
						<a href="img/hopelchen/san_fco_suc-tuc/guarniciones_banquetas/3.jpg" data-lightbox="Obra19"></a>
						<div class="titles">
							<h2>Pavimentación de Calles</h2>
							<p><em>H. Ayuntamiento de Hopelchen, Dirección de Obras Públicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>San Francisco Suc-Tuc</p>
					</div>
				</li>
				<li class="mix hopelchen xcupilcacab" data-name="hopelchen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra20" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Pavimentación de Calles</h2>
							<p><em>H. Ayuntamiento de Hopelchen, Dirección de Obras Públicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>Xcupil Cacab</p>
					</div>
				</li>
				<li class="mix hopelchen vicenteguerrero" data-name="hopelchen">
					<div class="meta name">
						<a href="img/hopelchen/vicente_guerrero/2.jpg" data-lightbox="Obra21">
						<div class="img_wrapper">
							<img src="img/hopelchen/vicente_guerrero/2.jpg" height="546" width="820" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/hopelchen/vicente_guerrero/1.jpg" data-lightbox="Obra21"></a>
						<div class="titles">
							<h2>Construcción de un Centro <br>Comunitario de Aprendizaje</h2>
							<p><em>H. Ayuntamiento de Hopelchen, Dirección de Obras Públicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>Vicente Guerrero (Iturbide)</p>
					</div>
				</li>
				<li class="mix hopelchen sancfcosuc-tuc" data-name="hopelchen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra22" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Ampliación de la Red de <br>Distribución de Energía Eléctrica</h2>
							<p><em>H. Ayuntamiento de Hopelchen, Dirección de Obras Públicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>San Francisco Suc-Tuc</p>
					</div>
				</li>
				<li class="mix hopelchen vicenteguerrero" data-name="hopelchen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra23" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Ampliación del Sistema de <br>Agua Potable (Rehabilitación)</h2>
							<p><em>H. Ayuntamiento de Hopelchen, Dirección de Obras Públicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>Vicente Guerrero (Iturbide)</p>
					</div>
				</li>
				<!-- OBRAS EN PALIZADA -->
				<li class="mix palizada islasanisidro" data-name="palizada">
					<div class="meta name">
						<a href="img/palizada/isla_sanisidro/5.jpg" data-lightbox="Obra24">
						<div class="img_wrapper">
							<img src="img/palizada/isla_sanisidro/5.jpg" height="480" width="640" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/palizada/isla_sanisidro/1.jpg" data-lightbox="Obra24"></a>
						<a href="img/palizada/isla_sanisidro/2.jpg" data-lightbox="Obra24"></a>
						<a href="img/palizada/isla_sanisidro/3.jpg" data-lightbox="Obra24"></a>
						<a href="img/palizada/isla_sanisidro/4.jpg" data-lightbox="Obra24"></a>
						<a href="img/palizada/isla_sanisidro/6.jpg" data-lightbox="Obra24"></a>
						<div class="titles">
							<h2>CONST. DE LAS CALLES ALDAMA, ORTIZ, <br>PIPILA, NIÑOS HEROES Y REFORMA  CON <br>CONCRETO HIDRAULICO</h2>
							<p><em>Direccion de Obras Publicas y Desarrollo Urbano</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Palizada</p>
					</div>
					<div class="meta rec">
						<p>Isla San Isidro</p>
					</div>
				</li>
				<li class="mix palizada" data-name="palizada">
					<div class="meta name">
						<a href="img/palizada/palizada/5.jpg" data-lightbox="Obra25">
						<div class="img_wrapper">
							<img src="img/palizada/palizada/5.jpg" height="519" width="691" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/palizada/palizada/1.jpg" data-lightbox="Obra25"></a>
						<a href="img/palizada/palizada/2.jpg" data-lightbox="Obra25"></a>
						<a href="img/palizada/palizada/3.jpg" data-lightbox="Obra25"></a>
						<a href="img/palizada/palizada/4.jpg" data-lightbox="Obra25"></a>
						<a href="img/palizada/palizada/6.jpg" data-lightbox="Obra25"></a>
						<div class="titles">
							<h2>AMPLIACION DE LA CASA DE LA CULTURA</h2>
							<p><em>Direccion de Obras Publicas y Desarrollo Urbano</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Palizada</p>
					</div>
					<div class="meta rec">
						<p>Palizada</p>
					</div>
				</li>
				<li class="mix palizada rioblanco" data-name="palizada">
					<div class="meta name">
						<a href="img/palizada/rio_blanco/4.jpg" data-lightbox="Obra26">
						<div class="img_wrapper">
							<img src="img/palizada/rio_blanco/4.jpg" height="506" width="674" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/palizada/rio_blanco/1.jpg" data-lightbox="Obra26"></a>
						<a href="img/palizada/rio_blanco/2.jpg" data-lightbox="Obra26"></a>
						<a href="img/palizada/rio_blanco/3.jpg" data-lightbox="Obra26"></a>
						<a href="img/palizada/rio_blanco/5.jpg" data-lightbox="Obra26"></a>
						<a href="img/palizada/rio_blanco/6.jpg" data-lightbox="Obra26"></a>
						<div class="titles">
							<h2>LIMPIEZA Y BACHEO DEL CAMINO RIO BLANCO</h2>
							<p><em>Direccion de Obras Publicas y Desarrollo Urbano</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Palizada</p>
					</div>
					<div class="meta rec">
						<p>Rio Blanco</p>
					</div>
				</li>
				<!-- OBRAS EN CALKINI -->
				<li class="mix calkini" data-name="calkini">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra27" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>RECONOCIMIENTO DE ESPACIOS <br>LIBRES DE HUMO DE TABACO</h2>
							<p><em>Secretaria de Salud. Programa de Adicciones</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calkinís</p>
					</div>
					<div class="meta rec">
						<p>Calkini</p>
					</div>
				</li>
				<li class="mix carmen" data-name="carmen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra28" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>RECONOCIMIENTO DE ESPACIOS <br>LIBRES DE HUMO DE TABACO</h2>
							<p><em>Secretaria de Salud. Programa de Adicciones</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Carmen</p>
					</div>
					<div class="meta rec">
						<p>Carmen</p>
					</div>
				</li>
				<li class="mix hopelchen" data-name="hopelchen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra29" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>RECONOCIMIENTO DE ESPACIOS <br>LIBRES DE HUMO DE TABACO</h2>
							<p><em>Secretaria de Salud. Programa de Adicciones</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>Hopelchén</p>
					</div>
				</li>
				</li>
				<li class="mix tenabo" data-name="tenabo">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra30" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>RECONOCIMIENTO DE ESPACIOS <br>LIBRES DE HUMO DE TABACO</h2>
							<p><em>Secretaria de Salud. Programa de Adicciones</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Tenabo</p>
					</div>
					<div class="meta rec">
						<p>Tenabo</p>
					</div>
				</li>
				<li class="mix calakmul centaurodelnorte" data-name="calakmul">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra31" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Sistema de captación de agua de lluvia para <br>consumo humano, compuesto por una olla<br> de almacenamiento recubierto con geomembrana</h2>
							<p><em>Secretaría de Desarrollo Rural</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Centauro del Norte</p>
					</div>
				</li>
				<li class="mix escarcega lagunagrande" data-name="escarcega">
					<div class="meta name">
						<a href="img/escarcega/laguna_grande/3.jpg" data-lightbox="Obra32">
						<div class="img_wrapper">
							<img src="img/escarcega/laguna_grande/3.jpg" height="477" width="636" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/escarcega/laguna_grande/1.jpg" data-lightbox="Obra32"></a>
						<a href="img/escarcega/laguna_grande/2.jpg" data-lightbox="Obra32"></a>
						<a href="img/escarcega/laguna_grande/4.jpg" data-lightbox="Obra32"></a>
						<a href="img/escarcega/laguna_grande/5.jpg" data-lightbox="Obra32"></a>
						<div class="titles">
							<h2>Caminos a zona de producción</h2>
							<p><em>Secretaría de Desarrollo Rural</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Escárcega</p>
					</div>
					<div class="meta rec">
						<p>Laguna Grande</p>
					</div>
				</li>
				<li class="mix candelaria miguelaleman" data-name="candelaria">
					<div class="meta name">
						<a href="img/candelaria/miguel_aleman/1.jpg" data-lightbox="Obra33">
						<div class="img_wrapper">
							<img src="img/candelaria/miguel_aleman/1.jpg" height="491" width="655" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/candelaria/miguel_aleman/2.jpg" data-lightbox="Obra33"></a>
						<a href="img/candelaria/miguel_aleman/3.jpg" data-lightbox="Obra33"></a>
						<a href="img/candelaria/miguel_aleman/4.jpg" data-lightbox="Obra33"></a>
						<a href="img/candelaria/miguel_aleman/5.jpg" data-lightbox="Obra33"></a>
						<a href="img/candelaria/miguel_aleman/6.jpg" data-lightbox="Obra33"></a>
						<div class="titles">
							<h2>Caminos a zona de producción</h2>
							<p><em>Secretaría de Desarrollo Rural</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Candelaria</p>
					</div>
					<div class="meta rec">
						<p>Miguel Alemán</p>
					</div>
				</li>
				<li class="mix campeche" data-name="campeche">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra34" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Entrega de Reembolsos (DIES) del <br>Programa Ganadero  y  Entrega de Equipos <br>a los productores ganaderos</h2>
							<p><em>Secretaría de Desarrollo Rural</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Campeche</p>
					</div>
					<div class="meta rec">
						<p>-</p>
					</div>
				</li>
				<li class="mix carmen josemaroiapinosuarez" data-name="carmen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra35" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Sistema de captación de agua de lluvia, <br>compuesto por una olla de almacenamiento <br>recubierta  con geomembrana</h2>
							<p><em>Secretaría de Desarrollo Rural</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Carmen</p>
					</div>
					<div class="meta rec">
						<p>José María Pino Suarez</p>
					</div>
				</li>
				<li class="mix calakmul nuevoparaiso" data-name="calakmul">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra36" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Sistema de captación de agua de lluvia para <br>consumo humano, compuesto por una olla <br>de almacenamiento  recubierta  con geomembrana</h2>
							<p><em>Secretaría de Desarrollo Rural</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Nuevo Paraíso</p>
					</div>
				</li>
				<li class="mix hopelchen xmaben" data-name="hopelchen">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra37" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Sistema de captación de agua de<br> lluvia compuesto por una olla de <br>almacenamiento  cubierta con geomembrana</h2>
							<p><em>Secretaría de Desarrollo Rural</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>X-maben</p>
					</div>
				</li>
				<li class="mix hopelchen crucerosanluis" data-name="hopelchen">
					<div class="meta name">
						<a href="img/hopelchen/crucero_san_luis/centro_computo/3.jpg" data-lightbox="Obra38">
						<div class="img_wrapper">
							<img src="img/hopelchen/crucero_san_luis/centro_computo/3.jpg" height="467" width="623" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/hopelchen/crucero_san_luis/centro_computo/1.jpg" data-lightbox="Obra38"></a>
						<a href="img/hopelchen/crucero_san_luis/centro_computo/2.jpg" data-lightbox="Obra38"></a>
						<div class="titles">
							<h2>Habilitar Área para Centro de Cómputo</h2>
							<p><em>Secretaria de Cultura</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>Crucero San Luis</p>
					</div>
				</li>
				<li class="mix tenabo emilianozapata" data-name="tenabo">
					<div class="meta name">
						<a href="img/tenabo/emiliano_zapata/5.jpg" data-lightbox="Obra39">
						<div class="img_wrapper">
							<img src="img/tenabo/emiliano_zapata/5.jpg" height="434" width="579" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/tenabo/emiliano_zapata/1.jpg" data-lightbox="Obra39"></a>
						<a href="img/tenabo/emiliano_zapata/2.jpg" data-lightbox="Obra39"></a>
						<a href="img/tenabo/emiliano_zapata/3.jpg" data-lightbox="Obra39"></a>
						<a href="img/tenabo/emiliano_zapata/4.jpg" data-lightbox="Obra39"></a>
						<a href="img/tenabo/emiliano_zapata/5.jpg" data-lightbox="Obra39"></a>
						<div class="titles">
							<h2>Habilitar Espacio para Biblioteca Pública<br> con Servicios Digitales</h2>
							<p><em>Secretaria de Cultura</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Tenabo</p>
					</div>
					<div class="meta rec">
						<p>Emiliano Zapata</p>
					</div>
				</li>
				<li class="mix champoton mayatecun1" data-name="champoton">
					<div class="meta name">
						<a href="img/champoton/mayatecun_1/6.jpg" data-lightbox="Obra40">
						<div class="img_wrapper">
							<img src="img/champoton/mayatecun_1/6.jpg" height="434" width="579" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/champoton/mayatecun_1/1.jpg" data-lightbox="Obra40"></a>
						<a href="img/champoton/mayatecun_1/2.jpg" data-lightbox="Obra40"></a>
						<a href="img/champoton/mayatecun_1/3.jpg" data-lightbox="Obra40"></a>
						<a href="img/champoton/mayatecun_1/4.jpg" data-lightbox="Obra40"></a>
						<a href="img/champoton/mayatecun_1/5.jpg" data-lightbox="Obra40"></a>
						<div class="titles">
							<h2>Habilitar Área para Centro de Cómputo</h2>
							<p><em>Secretaria de Cultura</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Mayatecun 1</p>
					</div>
				</li>
				<li class="mix champoton xbacab" data-name="champoton">
					<div class="meta name">
						<a href="img/champoton/xbacab/3.jpg" data-lightbox="Obra41">
						<div class="img_wrapper">
							<img src="img/champoton/xbacab/3.jpg" height="432" width="574" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/champoton/xbacab/1.jpg" data-lightbox="Obra41"></a>
						<a href="img/champoton/xbacab/2.jpg" data-lightbox="Obra41"></a>
						<a href="img/champoton/xbacab/4.jpg" data-lightbox="Obra41"></a>
						<a href="img/champoton/xbacab/5.jpg" data-lightbox="Obra41"></a>
						<a href="img/champoton/xbacab/6.jpg" data-lightbox="Obra41"></a>
						<div class="titles">
							<h2>Habilitar  espacio para  Biblioteca Pública</h2>
							<p><em>Secretaria de Cultura</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Xbacab</p>
					</div>
				</li>
				<li class="mix champoton yohaltun" data-name="champoton">
					<div class="meta name">
						<a href="img/champoton/yohaltun/3.jpg" data-lightbox="Obra42">
						<div class="img_wrapper">
							<img src="img/champoton/yohaltun/3.jpg" height="463" width="616" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/champoton/yohaltun/1.jpg" data-lightbox="Obra42"></a>
						<a href="img/champoton/yohaltun/2.jpg" data-lightbox="Obra42"></a>
						<a href="img/champoton/yohaltun/4.jpg" data-lightbox="Obra42"></a>
						<a href="img/champoton/yohaltun/5.jpg" data-lightbox="Obra42"></a>
						<a href="img/champoton/yohaltun/6.jpg" data-lightbox="Obra42"></a>
						<div class="titles">
							<h2>Habilitar Área para Instalación de Biblioteca Pública</h2>
							<p><em>Secretaria de Cultura</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Yohaltun</p>
					</div>
				</li>
				<li class="mix campeche" data-name="campeche">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra43" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Adquisición de vehículos para <br>la recolección de RSU (residuos sólidos<br> urbanos) en zona rural del municipio de<br>s san francisco de Campeche, Campeche</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Campeche</p>
					</div>
					<div class="meta rec">
						<p>Campeche</p>
					</div>
				</li>
				<li class="mix calakmul xpujil" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/xpujil/1.jpg" data-lightbox="Obra44">
						<div class="img_wrapper">
							<img src="img/calakmul/xpujil/1.jpg" height="470" width="613" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Construcción De Un Centro De Manejo <br>Integral De Residuos Sólidos Urbanos <br>En El Municipio De Calakmul</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Xpujil</p>
					</div>
				</li>
				<li class="mix calkini" data-name="calkini">
					<div class="meta name">
						<a href="img/calkini/1.jpg" data-lightbox="Obra45">
						<div class="img_wrapper">
							<img src="img/calkini/1.jpg" height="531" width="693" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>CONSTRUCCIÓN DE UN CENTRO DE MANEJO<br> INTEGRAL DE RESIDUOS SÓLIDOS URBANOS<br> EN EL MUNICIPIO DE CALKINI</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calkiní</p>
					</div>
					<div class="meta rec">
						<p>Calkiní</p>
					</div>
				</li>
				<li class="mix hecelchakan" data-name="hecelchakan">
					<div class="meta name">
						<a href="img/hecelchakan/1.jpg" data-lightbox="Obra46">
						<div class="img_wrapper">
							<img src="img/hecelchakan/1.jpg" height="531" width="693" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Construcción De Un Centro De Manejo Integral<br> De Residuos Sólidos Urbanos En <br>El Municipio De Hecelchakán</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hecelchakán</p>
					</div>
					<div class="meta rec">
						<p>Hecelchakán</p>
					</div>
				</li>
				<li class="mix hopelchen" data-name="hopelchen">
					<div class="meta name">
						<a href="img/hopelchen/1.jpg" data-lightbox="Obra47">
						<div class="img_wrapper">
							<img src="img/hopelchen/1.jpg" height="464" width="606" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Construcción De Un Centro De Manejo Integral<br> De Residuos Sólidos Urbanos En <br>El Municipio De Hopelchén</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hopelchén</p>
					</div>
					<div class="meta rec">
						<p>Hopelchén</p>
					</div>
				</li>
				<li class="mix campeche" data-name="campeche">
					<div class="meta name">
						<a href="img/campeche/1.jpg" data-lightbox="Obra48">
						<div class="img_wrapper">
							<img src="img/campeche/1.jpg" height="557" width="685" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/campeche/2.jpg" data-lightbox="Obra48"></a>
						<a href="img/campeche/3.jpg" data-lightbox="Obra48"></a>
						<div class="titles">
							<h2>Curso Internacional de Protección <br>contra Incendios Forestales.</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Campeche</p>
					</div>
					<div class="meta rec">
						<p>Campeche</p>
					</div>
				</li>
				<li class="mix calakmul xpujil" data-name="calakmul">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra49" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Entrega de equipamiento a<br> apicultores de Calakmul</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Xpujil/Diversas</p>
					</div>
				</li>
				<li class="mix " data-name="">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra50" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Equipamiento Al Sistema De Recolección <br>DeLos Municipios: Calkiní, Hecelchakán<br>, Hopelchén Y Calakmul</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>-</p>
					</div>
					<div class="meta rec">
						<p>-</p>
					</div>
				</li>
				<li class="mix calakmul becan" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/becan/1.jpg" data-lightbox="Obra51">
						<div class="img_wrapper">
							<img src="img/calakmul/becan/1.jpg" height="533" width="645" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/becan/2.jpg" data-lightbox="Obra51"></a>
						<a href="img/calakmul/becan/3.jpg" data-lightbox="Obra51"></a>
						<a href="img/calakmul/becan/4.jpg" data-lightbox="Obra51"></a>
						<a href="img/calakmul/becan/5.jpg" data-lightbox="Obra51"></a>
						<a href="img/calakmul/becan/6.jpg" data-lightbox="Obra51"></a>
						<div class="titles">
							<h2>Integración del proceso de manufactura<br> y  comercialización a la red de <br>valor miel orgánica en Calakmul</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Becan</p>
					</div>
				</li>
				<li class="mix campeche" data-name="campeche">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra52" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>Reforestacion  7 Parques</h2>
							<p><em>Secretaria de Medio Ambiente y Aprovechamiento Sustentable</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Campeche</p>
					</div>
					<div class="meta rec">
						<p>Campeche</p>
					</div>
				</li>
				<li class="mix hecelchakan" data-name="hecelchakan">
					<div class="meta name">
						<a href="img/hecelchakan/imagen_urbana/1.jpg" data-lightbox="Obra53">
						<div class="img_wrapper">
							<img src="img/hecelchakan/imagen_urbana/1.jpg" height="2448" width="3264" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/hecelchakan/imagen_urbana/2.jpg" data-lightbox="Obra53"></a>
						<a href="img/hecelchakan/imagen_urbana/3.jpg" data-lightbox="Obra53"></a>
						<a href="img/hecelchakan/imagen_urbana/4.jpg" data-lightbox="Obra53"></a>
						<a href="img/hecelchakan/imagen_urbana/5.jpg" data-lightbox="Obra53"></a>
						<a href="img/hecelchakan/imagen_urbana/6.jpg" data-lightbox="Obra53"></a>
						<div class="titles">
							<h2>IMAGEN URBANA DE HECELCHAKAN(REHABILITACIÓN)</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hecelchakán</p>
					</div>
					<div class="meta rec">
						<p>Hecelchakán</p>
					</div>
				</li>
				<li class="mix escarcega nuevocampeche" data-name="escarcega">
					<div class="meta name">
						<a href="img/escarcega/nuevo_campeche/5.jpg" data-lightbox="Obra54">
						<div class="img_wrapper">
							<img src="img/escarcega/nuevo_campeche/5.jpg" height="534" width="648" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/escarcega/nuevo_campeche/1.jpg" data-lightbox="Obra54"></a>
						<a href="img/escarcega/nuevo_campeche/2.jpg" data-lightbox="Obra54"></a>
						<a href="img/escarcega/nuevo_campeche/3.jpg" data-lightbox="Obra54"></a>
						<a href="img/escarcega/nuevo_campeche/4.jpg" data-lightbox="Obra54"></a>
						<a href="img/escarcega/nuevo_campeche/6.jpg" data-lightbox="Obra54"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DEL CAMINO LIBERTAD<br> - NUEVO CAMPECHE</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Escárcega</p>
					</div>
					<div class="meta rec">
						<p>Nuevo Campeche (Tres Aguadas)</p>
					</div>
				</li>
				<li class="mix champoton pixoyal" data-name="champoton">
					<div class="meta name">
						<a href="img/champoton/pixoyal/5.jpg" data-lightbox="Obra55">
						<div class="img_wrapper">
							<img src="img/champoton/pixoyal/5.jpg" height="3216" width="4288" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/champoton/pixoyal/1.jpg" data-lightbox="Obra55"></a>
						<a href="img/champoton/pixoyal/2.jpg" data-lightbox="Obra55"></a>
						<a href="img/champoton/pixoyal/3.jpg" data-lightbox="Obra55"></a>
						<a href="img/champoton/pixoyal/4.jpg" data-lightbox="Obra55"></a>
						<a href="img/champoton/pixoyal/6.jpg" data-lightbox="Obra55"></a>
						<div class="titles">
							<h2>REHABILITACION DE COMISARIA MUNICIPAL</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Pixoyal</p>
					</div>
				</li>
				<li class="mix calakmul xpujil" data-name="calakmul">
					<div class="meta name">
						<a href="img/calakmul/xpujil/3.1.jpg" data-lightbox="Obra56">
						<div class="img_wrapper">
							<img src="img/calakmul/xpujil/3.1.jpg" height="3216" width="4288" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calakmul/xpujil/1.1.jpg" data-lightbox="Obra56"></a>
						<a href="img/calakmul/xpujil/2.1.jpg" data-lightbox="Obra56"></a>
						<a href="img/calakmul/xpujil/4.1.jpg" data-lightbox="Obra56"></a>
						<a href="img/calakmul/xpujil/5.1.jpg" data-lightbox="Obra56"></a>
						<a href="img/calakmul/xpujil/6.1.jpg" data-lightbox="Obra56"></a>
						<div class="titles">
							<h2>CONSTRUCCION DE UN CENTRO DE MANEJO <br>INTEGRAL DE RESIDUOS SOLIDOS URBANOS</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calakmul</p>
					</div>
					<div class="meta rec">
						<p>Xpujil</p>
					</div>
				</li>
				<li class="mix champoton yohaltun" data-name="champoton">
					<div class="meta name">
						<a href="img/champoton/yohaltun/2.1.jpg" data-lightbox="Obra57">
						<div class="img_wrapper">
							<img src="img/champoton/yohaltun/2.1.jpg" height="3216" width="4288" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/champoton/yohaltun/1.1.jpg" data-lightbox="Obra57"></a>
						<a href="img/champoton/yohaltun/3.1.jpg" data-lightbox="Obra57"></a>
						<a href="img/champoton/yohaltun/4.1.jpg" data-lightbox="Obra57"></a>
						<a href="img/champoton/yohaltun/5.1.jpg" data-lightbox="Obra57"></a>
						<a href="img/champoton/yohaltun/6.1.jpg" data-lightbox="Obra57"></a>
						<div class="titles">
							<h2>CONTRUCCION DECOMISARIA MUNICIPAL</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Yohaltun</p>
					</div>
				</li>
				<li class="mix escarcega lagunagrande" data-name="escarcega">
					<div class="meta name">
						<a href="img/escarcega/laguna_grande/3.1.jpg" data-lightbox="Obra58">
						<div class="img_wrapper">
							<img src="img/escarcega/laguna_grande/3.1.jpg" height="520" width="693" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/escarcega/laguna_grande/1.1.jpg" data-lightbox="Obra58"></a>
						<a href="img/escarcega/laguna_grande/2.1.jpg" data-lightbox="Obra58"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DE CALLES EN LAGUNA GRANDE</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Escárcega</p>
					</div>
					<div class="meta rec">
						<p>Laguna Grande</p>
					</div>
				</li>
				<li class="mix champoton miguelcolorado" data-name="champoton">
					<div class="meta name">
						<a href="img/champoton/miguel_colorado/3.jpg" data-lightbox="Obra59">
						<div class="img_wrapper">
							<img src="img/champoton/miguel_colorado/3.jpg" height="520" width="693" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/champoton/miguel_colorado/1.jpg" data-lightbox="Obra59"></a>
						<a href="img/champoton/miguel_colorado/2.jpg" data-lightbox="Obra59"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DE CALLES EN MIGUEL COLORADO</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Miguel Colorado</p>
					</div>
				</li>
				<li class="mix escarcega migueldelamadrid" data-name="escarcega">
					<div class="meta name">
						<a href="img/escarcega/miguel_de_la_madrid/5.jpg" data-lightbox="Obra60">
						<div class="img_wrapper">
							<img src="img/escarcega/miguel_de_la_madrid/5.jpg" height="479" width="662" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/escarcega/miguel_de_la_madrid/1.jpg" data-lightbox="Obra60"></a>
						<a href="img/escarcega/miguel_de_la_madrid/2.jpg" data-lightbox="Obra60"></a>
						<a href="img/escarcega/miguel_de_la_madrid/3.jpg" data-lightbox="Obra60"></a>
						<a href="img/escarcega/miguel_de_la_madrid/4.jpg" data-lightbox="Obra60"></a>
						<a href="img/escarcega/miguel_de_la_madrid/6.jpg" data-lightbox="Obra60"></a>
						<div class="titles">
							<h2>MODERNIZACION Y AMPLIACION DEL CAMINO<br> BELEN – LICENCIADO MIGUEL DE LA MADRID,<br> TRAMO: DEL KM 0+000 AL KM 9+500, SUBTRAMO <br> MODERNIZAR DEL KM 0+000 AL KM 9+500</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Escárcega</p>
					</div>
					<div class="meta rec">
						<p>Lic Miguel de la Madrid</p>
					</div>
				</li>
				<li class="mix candelaria guadalupevictoria" data-name="candelaria">
					<div class="meta name">
						<a href="img/candelaria/guadalupe_victoria/2.jpg" data-lightbox="Obra61">
						<div class="img_wrapper">
							<img src="img/candelaria/guadalupe_victoria/2.jpg" height="527" width="635" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/candelaria/guadalupe_victoria/1.jpg" data-lightbox="Obra61"></a>
						<a href="img/candelaria/guadalupe_victoria/3.jpg" data-lightbox="Obra61"></a>
						<a href="img/candelaria/guadalupe_victoria/4.jpg" data-lightbox="Obra61"></a>
						<a href="img/candelaria/guadalupe_victoria/5.jpg" data-lightbox="Obra61"></a>
						<a href="img/candelaria/guadalupe_victoria/6.jpg" data-lightbox="Obra61"></a>
						<div class="titles">
							<h2>RECONSTRUCCIÓN DEL CAMINO BENITO JUÁREZ - <br>GUADALUPE VICTORIA ( 1ER. PRESIDENTE DE <br>MEXICO) TRAMO KM 0+000 AL KM 8+000, Subtramo<br> a modernizar: km 0+000 al km 8+000</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Candelaria</p>
					</div>
					<div class="meta rec">
						<p>Guadalupe Victoria</p>
					</div>
				</li>
				<li class="mix calkini" data-name="calkini">
					<div class="meta name">
						<a href="img/calkini/3.1.jpg" data-lightbox="Obra62">
						<div class="img_wrapper">
							<img src="img/calkini/3.1.jpg" height="3216" width="4288" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calkini/1.1.jpg" data-lightbox="Obra62"></a>
						<a href="img/calkini/2.1.jpg" data-lightbox="Obra62"></a>
						<a href="img/calkini/4.1.jpg" data-lightbox="Obra62"></a>
						<a href="img/calkini/5.1.jpg" data-lightbox="Obra62"></a>
						<a href="img/calkini/6.1.jpg" data-lightbox="Obra62"></a>
						<div class="titles">
							<h2>CONSTRUCCION DE UN CENTRO DE MANEJO<br> INTEGRAL DE RESIDUOS SOLIDOS SOLIDOS<br> URBANOS EN EL MUNICIPIO DE CALKINI</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calkiní</p>
					</div>
					<div class="meta rec">
						<p>Calkiní</p>
					</div>
				</li>
				<li class="mix champoton" data-name="champoton">
					<div class="meta name">
						<a href="img/champoton/1.jpg" data-lightbox="Obra63">
						<div class="img_wrapper">
							<img src="img/champoton/1.jpg" height="2448" width="3264" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/champoton/2.jpg" data-lightbox="Obra63"></a>
						<a href="img/champoton/3.jpg" data-lightbox="Obra63"></a>
						<a href="img/champoton/4.jpg" data-lightbox="Obra63"></a>
						<a href="img/champoton/5.jpg" data-lightbox="Obra63"></a>
						<a href="img/champoton/6.jpg" data-lightbox="Obra63"></a>
						<div class="titles">
							<h2>CANCHA DE USOS MULPLES (CONSTRUCCION)</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Champotón</p>
					</div>
				</li>
				<li class="mix carmen" data-name="carmen">
					<div class="meta name">
						<a href="img/carmen/1.jpg" data-lightbox="Obra64">
						<div class="img_wrapper">
							<img src="img/carmen/1.jpg" height="1536" width="2048" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/carmen/2.jpg" data-lightbox="Obra64"></a>
						<a href="img/carmen/3.jpg" data-lightbox="Obra64"></a>
						<a href="img/carmen/4.jpg" data-lightbox="Obra64"></a>
						<a href="img/carmen/5.jpg" data-lightbox="Obra64"></a>
						<a href="img/carmen/6.jpg" data-lightbox="Obra64"></a>
						<div class="titles">
							<h2>CENTRO DE JUSTICIA DE LA MUJER</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Carmen</p>
					</div>
					<div class="meta rec">
						<p>Carmen</p>
					</div>
				</li>
				<li class="mix campeche" data-name="campeche">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra65" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>CENDI ISSSTECAM, CAMPECHE</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Campeche</p>
					</div>
					<div class="meta rec">
						<p>Campeche</p>
					</div>
				</li>
				<li class="mix candelaria pablogarcia" data-name="candelaria">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra66" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>CONSTRUCCION DE COMISARIA EJIDAL</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Candelaria</p>
					</div>
					<div class="meta rec">
						<p>Pablo Garcia</p>
					</div>
				</li>
				<li class="mix escarcega miguelhidalgoycostilla" data-name="candelaria">
					<div class="meta name">
						<a href="img/escarcega/miguel_hidalgo_y_constrilla/3.jpg" data-lightbox="Obra67">
						<div class="img_wrapper">
							<img src="img/escarcega/miguel_hidalgo_y_constrilla/3.jpg" height="494" width="658" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/escarcega/miguel_hidalgo_y_constrilla/1.jpg" data-lightbox="Obra67"></a>
						<a href="img/escarcega/miguel_hidalgo_y_constrilla/2.jpg" data-lightbox="Obra67"></a>
						<a href="img/escarcega/miguel_hidalgo_y_constrilla/4.jpg" data-lightbox="Obra67"></a>
						<a href="img/escarcega/miguel_hidalgo_y_constrilla/5.jpg" data-lightbox="Obra67"></a>
						<a href="img/escarcega/miguel_hidalgo_y_constrilla/6.jpg" data-lightbox="Obra67"></a>
						<div class="titles">
							<h2>RECONSTRUCCIÓN DEL CAMINO MATAMOROS –<br>MIGUEL HIDALGO Y COSTILLA ( EL CARACOL)</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Escárcega</p>
					</div>
					<div class="meta rec">
						<p>Miguel Hidalgo y Costilla  (el Caracol)</p>
					</div>
				</li>
				<li class="mix hecelchakan" data-name="hecelchakan">
					<div class="meta name">
						<a href="img/hecelchakan/residuos/2.jpg" data-lightbox="Obra68">
						<div class="img_wrapper">
							<img src="img/hecelchakan/residuos/2.jpg" height="3216" width="4288" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/hecelchakan/residuos/1.jpg" data-lightbox="Obra68"></a>
						<a href="img/hecelchakan/residuos/3.jpg" data-lightbox="Obra68"></a>
						<a href="img/hecelchakan/residuos/4.jpg" data-lightbox="Obra68"></a>
						<a href="img/hecelchakan/residuos/5.jpg" data-lightbox="Obra68"></a>
						<a href="img/hecelchakan/residuos/6.jpg" data-lightbox="Obra68"></a>
						<div class="titles">
							<h2>CONSTRUCCION DE UN CENTRO DE MANEJO<br> INTEGRAL DE RESIDUOS SOLIDOS SOLIDOS <br>URBANOS EN EL MUNICIPIO DE HECELCHAKAN</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hecelchakán</p>
					</div>
					<div class="meta rec">
						<p>Hecelchakán</p>
					</div>
				</li>
				<li class="mix carmen juandelacabadavera" data-name="carmen">
					<div class="meta name">
						<a href="img/carmen/juan_de_la_cabada/5.jpg" data-lightbox="Obra69">
						<div class="img_wrapper">
							<img src="img/carmen/juan_de_la_cabada/5.jpg" height="480" width="640" onload="imgLoaded(this)"/>
						</div>
						</a>	
						<a href="img/carmen/juan_de_la_cabada/1.jpg" data-lightbox="Obra69"></a>
						<a href="img/carmen/juan_de_la_cabada/2.jpg" data-lightbox="Obra69"></a>
						<a href="img/carmen/juan_de_la_cabada/3.jpg" data-lightbox="Obra69"></a>
						<a href="img/carmen/juan_de_la_cabada/4.jpg" data-lightbox="Obra69"></a>
						<a href="img/carmen/juan_de_la_cabada/6.jpg" data-lightbox="Obra69"></a>
						<div class="titles">
							<h2>RECONSTRUCCIÓN DEL CAMINO E.C. VHSA-<br> ESC) KM 203+000 – JUAN DE LA CABADA VERA;<br> TRAMO: DEL KM 0+000 KM 6+080, SUBTRAMO:<br> DEL KM 0+000 AL 6+080</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Carmen</p>
					</div>
					<div class="meta rec">
						<p>Juan de la Cabada Vera</p>
					</div>
				</li>
				<li class="mix hecelchakan pomuch" data-name="hecelchakan">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra70" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>UNIDAD DEPORTIVA (CONSTRUCCION) POMUCH</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Hecelchakán</p>
					</div>
					<div class="meta rec">
						<p>Pomuch</p>
					</div>
				</li>
				<li class="mix escarcega luna" data-name="escarcega">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra71" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>REHABILITACION DE COMISARIA EJIDAL</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Escárcega</p>
					</div>
					<div class="meta rec">
						<p>Luna</p>
					</div>
				</li>
				<li class="mix candelaria pablogarcia" data-name="candelaria">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra72" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>REMODELACION DE COMISARIA MUNICIPAL</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Candelaria</p>
					</div>
					<div class="meta rec">
						<p>Pablo Garcia</p>
					</div>
				</li>
				<li class="mix champoton revolucion" data-name="champoton">
					<div class="meta name">
						<a href="img/foto-no-disponible.jpg" data-lightbox="Obra73" title="No hay images para la obra por el momento">
						<div class="img_wrapper">
							<img src="img/foto-no-disponible.jpg" height="257" width="449" onload="imgLoaded(this)"/>
						</div>
						</a>
						<div class="titles">
							<h2>REMODELACION DE COMISARIA MUNICIPAL</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Revolución</p>
					</div>
				</li>
				<li class="mix carmen sanmiguelallende" data-name="carmen">
					<div class="meta name">
						<a href="img/carmen/san_miguel_allende/1.jpg" data-lightbox="Obra74">
						<div class="img_wrapper">
							<img src="img/carmen/san_miguel_allende/1.jpg" height="800" width="450" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/carmen/san_miguel_allende/2.jpg" data-lightbox="Obra74"></a>
						<a href="img/carmen/san_miguel_allende/3.jpg" data-lightbox="Obra74"></a>
						<a href="img/carmen/san_miguel_allende/4.jpg" data-lightbox="Obra74"></a>
						<a href="img/carmen/san_miguel_allende/5.jpg" data-lightbox="Obra74"></a>
						<a href="img/carmen/san_miguel_allende/6.jpg" data-lightbox="Obra74"></a>
						<div class="titles">
							<h2>RECONSTRUCCIÓN DEL CAMINO E.C GUADALUPE<br> VICTORIA – EL FAISAN- SAN MIGUEL ALLENDE TRAMO<br> KM 0+000 AL KM 1+000, Subtramo a <br>modernizar: km 0+000 al km 1+000</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Carmen</p>
					</div>
					<div class="meta rec">
						<p>San Miguel Allende</p>
					</div>
				</li>
				<li class="mix champoton" data-name="champoton">
					<div class="meta name">
						<a href="img/champoton/ulises_sansores/3.jpg" data-lightbox="Obra75">
						<div class="img_wrapper">
							<img src="img/champoton/ulises_sansores/3.jpg" height="2448" width="3264" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/champoton/ulises_sansores/1.jpg" data-lightbox="Obra75"></a>
						<a href="img/champoton/ulises_sansores/2.jpg" data-lightbox="Obra75"></a>
						<a href="img/champoton/ulises_sansores/4.jpg" data-lightbox="Obra75"></a>
						<a href="img/champoton/ulises_sansores/5.jpg" data-lightbox="Obra75"></a>
						<a href="img/champoton/ulises_sansores/6.jpg" data-lightbox="Obra75"></a>
						<div class="titles">
							<h2>REHABILITACION UNIDAD DEPORTIVA ULISES SANSORES</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Champotón</p>
					</div>
					<div class="meta rec">
						<p>Champotón</p>
					</div>
				</li>
				<li class="mix calkini xcacoch" data-name="calkini">
					<div class="meta name">
						<a href="img/calkini/xcacoch/1.jpg" data-lightbox="Obra76">
						<div class="img_wrapper">
							<img src="img/calkini/xcacoch/1.jpg" height="487" width="649" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/calkini/xcacoch/2.jpg" data-lightbox="Obra76"></a>
						<a href="img/calkini/xcacoch/3.jpg" data-lightbox="Obra76"></a>
						<a href="img/calkini/xcacoch/4.jpg" data-lightbox="Obra76"></a>
						<a href="img/calkini/xcacoch/5.jpg" data-lightbox="Obra76"></a>
						<a href="img/calkini/xcacoch/6.jpg" data-lightbox="Obra76"></a>
						<div class="titles">
							<h2>MODERNIZACIÓN Y AMPLIACIÓN DEL CAMINO E.C.<br> (STA. CRUZ - CHUNHUAS) KM 6+180 – XCACOCH, <br>TRAMO: DEL KM 0+000 AL KM 2+000</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Calkiní</p>
					</div>
					<div class="meta rec">
						<p>Xcacoch</p>
					</div>
				</li>
				<li class="mix candelaria" data-name="candelaria">
					<div class="meta name">
						<a href="img/candelaria/2.jpg" data-lightbox="Obra77">
						<div class="img_wrapper">
							<img src="img/candelaria/2.jpg" height="520" width="693" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/candelaria/1.jpg" data-lightbox="Obra77"></a>
						<a href="img/candelaria/3.jpg" data-lightbox="Obra77"></a>
						<a href="img/candelaria/4.jpg" data-lightbox="Obra77"></a>
						<a href="img/candelaria/5.jpg" data-lightbox="Obra77"></a>
						<a href="img/candelaria/6.jpg" data-lightbox="Obra77"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DE LAS CALLES EN CANDELARIA</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Candelaria</p>
					</div>
					<div class="meta rec">
						<p>Candelaria</p>
					</div>
				</li>
				<li class="mix escarcega silvituc" data-name="escarcega">
					<div class="meta name">
						<a href="img/escarcega/silvituc/1.jpg" data-lightbox="Obra78">
						<div class="img_wrapper">
							<img src="img/escarcega/silvituc/1.jpg" height="520" width="693" onload="imgLoaded(this)"/>
						</div>
						</a>
						<a href="img/escarcega/silvituc/2.jpg" data-lightbox="Obra78"></a>
						<a href="img/escarcega/silvituc/3.jpg" data-lightbox="Obra78"></a>
						<div class="titles">
							<h2>RECONSTRUCCION DE CALLES EN SILVITUC</h2>
							<p><em>Secretaria de Desarrollo Urbano y Obras Publicas</em></p>
						</div>
					</div>
					<div class="meta region">
						<p>Escárcega</p>
					</div>
					<div class="meta rec">
						<p>Silvituc</p>
					</div>
				</li>



					
				<!-- END LIST OF OBRAS -->
			</ul>

		</div>
		
		<!-- END DEMO WRAPPER -->
		
		</section>
		
		<!-- BEGIN FOOTER -->
		
		<footer class="wf">
				<div class="left">
					<img src="img/campeche.png" height="120" width="220"/>
				</div>
				<div class="right">
					<p>Gobierno del Estado de Campeche</p>
					<p class="small">&copy; Copyright <? echo(Date("Y")); ?> Secretaría de Coordinación.<br> Todos los Derechos Reservados.</p>
				</div>
			<div class="clear"></div>
		</footer>
		
		<!-- END FOOTER -->
		
		<script>
		</script>
		
	</body>
</html>

